import { Alert, Button, CircularProgress, Paper, Snackbar, TextField } from '@mui/material'
import React, { useEffect, useState, useCallback } from 'react'
import './app.scss'
import Logo from './assets/logo.png'

const defaultItems = ['A', 'B', 'C', 'D', 'E']
let current = defaultItems
let collections: Array<string> = []

const rotateItems = (stateHandler: React.Dispatch<React.SetStateAction<string[]>>) => {
  let first = current[0]

  if (collections.length) {
    first = collections[0]
    collections = [...collections.slice(1)]
  }

  const rest = current.slice(1)
  current = [...rest, first]
  stateHandler(current)
}

const resetItems = () => {
  collections = []
  current = defaultItems
}

let cron: ReturnType<typeof setInterval>

const App = () => {
  const [items, setItems] = useState(defaultItems)
  const [search, setSearch] = useState('')
  const [error, setError] = useState(false)
  const [isFetching, setIsFetching] = useState(false)

  const fetchApi = useCallback((search:string) => {
    setIsFetching(true)
    resetItems()
    fetch(`/api/get-collections?search=${search}`)
      .then(res => {
        if (res.status >= 400) {
          setError(true)
        }
        return res.json()
      })
      .then(({ data }) => {
        collections = data
      })
      .finally(() => {
        setIsFetching(false)
      })
  }, [])

  useEffect(() => {
    cron = setInterval(() => rotateItems(setItems), 1000)
    return () => {
      clearInterval(cron)
    }
  }, [])

  return (
    <div className="container">
      <img src={Logo} alt="Tricentis logo"/>
      <TextField
          value={search}
          label="Search album"
          onChange={e => setSearch(e.currentTarget.value)}
          onKeyDown={e => { if (e.key === 'Enter') { fetchApi(search) } }}
        />
      <div className='button-container'>
        <Button
        className="button"
        variant="contained"
        color="primary"
        onClick={() => fetchApi(search)}>
          Search
        </Button>
        <Button
        className="button"
        variant="contained"
        onClick={() => {
          setSearch('')
          resetItems()
        }}>
          Clear
        </Button>
      </div>
      <div className='item-container'>
        {isFetching && <CircularProgress size={60}/>}
        {!isFetching && items.map((item, i) => <Paper className='item' key={i} elevation={1}>{item}</Paper>)}
      </div>

      <Snackbar
      open={error}
      onClose={() => setError(false)}
      autoHideDuration={6000}
      >
        <Alert severity='error'>
          Error occured. Please try again later
        </Alert>
      </Snackbar>
    </div>
  )
}

export default App
