require('dotenv').config()
const path = require('path')
const axios = require('axios')
const express = require('express')

const port = process.env.PORT || 4500
const app = express()

app.use(express.static(path.join(__dirname, '../public/')))

app.get('/api/get-collections', async (req, res) => {
  const { search } = req.query
  try {
    const { data } = await axios(`https://itunes.apple.com/search?term=${search}`)
    const collectionNames = data.results.map(d => d.collectionName)
    const uniqCollectionNames = [...new Set(collectionNames)]
    const result = uniqCollectionNames.sort().slice(0, 5)
    return res.status(200).send({ data: result })
  } catch (error) {
    return res.status(500).send({ data: [], error })
  }
})

app.listen(port, () => {
  console.info(`Express listening at http://localhost:${port}`)
  console.info(`serving bundled content from ${path.join(__dirname, '../public')}`)
})
